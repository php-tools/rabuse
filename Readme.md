# RAbuse
**R**uby Abuse scanner.

CLI tool used to interface with PHP parser, formatter, deobfuscator and rule matchers.

## Usage
```
./rabuse
```
The script will show help options when called without any parameters. If you need more information on a specific command you can request the help for a specific command
```
./rabuse help scan
```
Example of starting an interactive scan.
```
./rabuse scan --interactive /path/to/scan/
```
Most commands and parameters can be shortened. The command below does the same as the command above.
```
./rabuse s -i /path/to/scan
```
Commands (ie. `scan`) may be arbitrarily shortened so long as no ambiguity exists between commands. For example if there are multiple commands starting with "s", you will need to specify more of the `scan` command to prevent a naming collision.

The parameters to a command may not be shortened arbitrarily. The `help` for the command will show you the short form of the parameter if available.

Example of **INVALID** parameter short form;
```
./rabuse scan --inte /path/to/scan
```
This "short form" for the `--interactive` parameter is not allowed and will produce an error.
